//
// MappingManager.java
//
//     MappingManager for Location Casino.
//
//
// Tom Coppeto
// 27 July 2019
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.examples.providers.mapping.roulette;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


public final class MappingManager
    extends net.okapia.osid.jamocha.mapping.spi.AbstractMappingManager
    implements org.osid.mapping.MappingManager {

    private static final org.osid.id.Id BANKROLL_PARAMETER = net.okapia.osid.primordium.id.BasicId.valueOf("configuration:bankroll@local");
    private static final org.osid.id.Id MINBET_PARAMETER = net.okapia.osid.primordium.id.BasicId.valueOf("configuration:minimumBet@local");

    private static final org.osid.mapping.Map MAP = new net.okapia.osid.jamocha.builder.mapping.map.MapBuilder()
        .current()
        .id(net.okapia.osid.primordium.id.URNId.valueOf("urn:osid:okapia.net:identifiers:map:Monte Carlo"))
        .displayName(text("Monte Carlo"))
        .description(text("Do you feel lucky?"))
        .provider(new ServiceProvider().getProvider())
        .build();

    private Configuration configuration;


    /**
     *  Constructs a @{code MappingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider}
     *          is <code>null</code>
     */

    public MappingManager() {
        super(new ServiceProvider());
        return;
    }


    /**
     *  Initializes this manager. A manager is initialized once at the time of
     *  creation.
     *
     *  @param  runtime the runtime environment
     *  @throws org.osid.ConfigurationErrorException an error with
     *          implementation configuration
     *  @throws org.osid.IllegalStateException this manager has already been
     *          initialized by the <code> OsidLoader </code>
     *  @throws org.osid.NullArgumentException <code> runtime </code> is
     *  <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        super.initialize(runtime);
        this.configuration = new Configuration();

        return;
    }


    /**
     *  Tests if looking up locations is supported. 
     *
     *  @return <code> true </code> if location lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationLookup() {
        return (true);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  lookup service. 
     *
     *  @return a <code> LocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationLookupSession getLocationLookupSession()
        throws org.osid.OperationFailedException {

        return (new LocationLookupSession(MAP, this.configuration.getBankroll(), this.configuration.getTable()));
    }


    class Configuration {
        private int bankrollAmount = 20;
        private int minimumBet = 5;

        
        Configuration() 
            throws org.osid.ConfigurationErrorException,
                   org.osid.OperationFailedException {

            try {
                this.bankrollAmount = (int) getConfigurationValue(BANKROLL_PARAMETER).getCardinalValue();
            } catch (org.osid.OsidException oe) {}

            try {
                this.minimumBet = (int) getConfigurationValue(MINBET_PARAMETER).getCardinalValue();
            } catch (org.osid.OsidException oe) {}

            return;
        }

        
        Bankroll getBankroll() {
            return (new Bankroll(this.bankrollAmount));
        }


        Table getTable() {
            return (new Table(this.minimumBet));
        }
    }

            
    static class Bankroll {
        private int amount;

        
        Bankroll(int amount) {
            this.amount = amount;
            return;
        }


        int getAmount() {
            return (this.amount);
        }

        
        boolean spend(int amount) {
            cardinalarg(amount, "spend amount");

            if (amount > this.amount) {
                return (false);
            }

            this.amount -= amount;
            return (true);
        }
    }

    
    static class Table {
        private int minBet;

        
        Table(int minBet) {
            cardinalarg(minBet, "minimum bet");
            this.minBet = minBet;
            return;
        }


        int getMinimumBet() {
            return (this.minBet);
        }
    }
}
