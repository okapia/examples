//
// MappingManager.java
//
//     MappingManager for my home.
//
//
// Tom Coppeto
// 27 July 2019
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.examples.providers.mapping.worldatlas;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  
 */

public final class MappingManager
    extends net.okapia.osid.jamocha.mapping.spi.AbstractMappingManager
    implements org.osid.mapping.MappingManager {

    private static final org.osid.mapping.Map MAP = new FederatedMap();
    private static final org.osid.id.Id PROVIDER_PARAMETER_ID = net.okapia.osid.primordium.id.BasicId.valueOf("configuration:provider@local");    

    private final java.util.Map<org.osid.id.Id, org.osid.mapping.MappingManager> providers = new java.util.HashMap<>();


    public MappingManager() {
        super(new ServiceProvider());
        return;
    }


    /**
     *  Initializes this manager. A manager is initialized once at the
     *  time of creation.
     *
     *  @param  runtime the runtime environment 
     *  @throws org.osid.ConfigurationErrorException an error with 
     *          implementation configuration 
     *  @throws org.osid.IllegalStateException this manager has already been 
     *          initialized by the <code> OsidRuntime </code> 
     *  @throws org.osid.NullArgumentException <code> runtime </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */
    
    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        super.initialize(runtime);
        
        try (org.osid.configuration.ValueList values = getConfigurationValues(PROVIDER_PARAMETER_ID)) {
            while (values.hasNext()) {
                org.osid.mapping.MappingManager mgr = (org.osid.mapping.MappingManager) getOsidManager(org.osid.OSID.MAPPING, 
                                                                                                                       values.getNextValue().getStringValue());
                
                /* 
                 * Notice the assumption I put on the provider Id. What if
                 * they collided? This is something I will have to deal
                 * with when the problem pops up, and I either add some
                 * salt to the key or put an adapter on top of the
                 * underlying OsidManager and change its Id (this is an
                 * issue across all applications of the federating
                 * pattern).
                 */
                
                this.providers.put(mgr.getId(), mgr);
            }
        }
    
        return;
    }
   

    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is
     *          supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (true);
    }


    /**
     *  Tests if an location lookup service is supported. An location lookup service 
     *  defines methods to access locations. 
     *
     *  @return true if location lookup is supported, false otherwise 
     */
    
    @OSID @Override
    public boolean supportsLocationLookup() {

        /*
         * I have a choice here. I can always say yes in which case
         * the federator never returns anything. Or, as shown here,
         * this adapter turns off support if none of the underlying
         * OSID Providers support it.
         */

        for (org.osid.mapping.MappingManager mgr : this.providers.values()) {
            if (mgr.supportsLocationLookup()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location lookup 
     *  service. 
     *
     *  @return an <code> LocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLocationLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationLookupSession getLocationLookupSession()
        throws org.osid.OperationFailedException {

        /*
         * I can't pass this off to
         * getLocationLookupSessionForMap(). Each underlying OSID
         * Provider has a different set of Maps so this will rely
         * on their defaults.
         */

        net.okapia.osid.jamocha.adapter.federator.mapping.FederatingLocationLookupSession session =
            new net.okapia.osid.jamocha.adapter.federator.mapping.FederatingLocationLookupSession(MAP);

        for (org.osid.mapping.MappingManager mgr : this.providers.values()) {
            if (mgr.supportsLocationLookup()) {
                session.addSession(mgr.getLocationLookupSession());
            }
        }

        return (session);                
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location lookup 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return an <code> LocationLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> mapId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsLocationLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationLookupSession getLocationLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        /*
         * The logic here gets screwy. If our federated Map is
         * requested, I may as well pass it back to
         * getLocationLookupSession(). Otherwise, it is likely that the
         * map Id is only known to subset, if not just one, OSID
         * Provider.
         */

        if (MAP.getId().equals(mapId)) {
            return (getLocationLookupSession());
        }

        /*
         * I can leverage the federated getMap() method to help me out.
         */

        org.osid.mapping.Map map;

        /* honoring the NotFoundException */
        try {
            map = getMapLookupSession().getMap(mapId);
        } catch (org.osid.PermissionDeniedException pde) {
            throw new org.osid.OperationFailedException(pde);
        }

        net.okapia.osid.jamocha.adapter.federator.mapping.FederatingLocationLookupSession session =
            new net.okapia.osid.jamocha.adapter.federator.mapping.FederatingLocationLookupSession(map);

        for (org.osid.mapping.MappingManager mgr : this.providers.values()) {
            if (mgr.supportsLocationLookup() && mgr.supportsVisibleFederation()) {
                try {
                    session.addSession(mgr.getLocationLookupSessionForMap(mapId));
                } catch (org.osid.NotFoundException nfe) {
                    // mapId doesn't exist there
                }
            }
        }

        return (session);        
    }


    /**
     *  Tests if a map lookup service is supported. 
     *
     *  @return <code> true </code> if map lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapLookup() {
        return (true); /* we always have one of our own */
    }

    
    /**
     *  Gets the OsidSession associated with the map lookup service. 
     *
     *  @return a <code> MapLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMapLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapLookupSession getMapLookupSession()
        throws org.osid.OperationFailedException {

       net.okapia.osid.jamocha.adapter.federator.mapping.FederatingMapLookupSession session =
           new net.okapia.osid.jamocha.adapter.federator.mapping.FederatingMapLookupSession();

       /* don't forget ours */
       session.addSession(new net.okapia.osid.jamocha.core.mapping.InvariantMapMapLookupSession(MAP));

       /*
        * What if this OsidSession is not supported but don't we want
        * to see one anyway?  I may wish to solve here (create a
        * Map that maps to the whole provider) or address in a
        * separate adapter.
        *
        * Because the folks at TomCal figured it out by e03, I don't
        * have to worry about it (yet).
        */

       for (org.osid.mapping.MappingManager mgr : this.providers.values()) {
           if (mgr.supportsMapLookup()) {
               session.addSession(mgr.getMapLookupSession());
           }
       }

       return (session);
    }
}

