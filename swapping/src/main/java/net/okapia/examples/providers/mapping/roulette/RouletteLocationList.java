//
// RouletteLocationList.java
//
//     Implements a Roulette-based LocationList.
//
//
// Tom Coppeto
// 27 July 2019
//
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.examples.providers.mapping.roulette;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;

import net.okapia.examples.providers.mapping.roulette.MappingManager.Bankroll;
import net.okapia.examples.providers.mapping.roulette.MappingManager.Table;


final class RouletteLocationList
    extends net.okapia.osid.jamocha.mapping.location.spi.AbstractLocationList
    implements org.osid.mapping.LocationList {

    private final Bankroll bankroll;
    private final Table table;


    RouletteLocationList(Bankroll bankroll, Table table) {
        nullarg(bankroll, "bankroll");
        nullarg(table, "table");

        this.bankroll = bankroll;
        this.table = table;

        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (this.bankroll.getAmount() >= this.table.getMinimumBet()) {
            return (true);
        }

        return (false);
    }


    /**
     *  Gets the number of elements available for retrieval. The number 
     *  returned by this method may be less than or equal to the total number 
     *  of elements in this list. To determine if the end of the list has been 
     *  reached, the method <code> hasNext() </code> should be used. This 
     *  method conveys what is known about the number of remaining elements at 
     *  a point in time and can be used to determine a minimum size of the 
     *  remaining elements, if known. A valid return is zero even if <code> 
     *  hasNext() </code> is true. 
     *  <p>
     *  This method does not imply asynchronous usage. All OSID methods may 
     *  block. 
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        return (this.table.getMinimumBet() / this.bankroll.getAmount());
    }


    /**
     *  Gets the next <code>Location</code> in this list. 
     *
     *  @return the next <code>Location</code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code>Location</code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getNextLocation()
        throws org.osid.OperationFailedException {

        if (hasNext()) {
            this.bankroll.spend(this.table.getMinimumBet());
            return (RouletteLocation.spin());
        } else {
            throw new org.osid.IllegalStateException("no more elements available in location list");
        }
    }
}
