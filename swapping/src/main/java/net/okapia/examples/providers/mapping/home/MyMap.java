//
// MyMap.java
//
//     My map.
//
//
// Tom Coppeto
// 27 July 2019
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.examples.providers.mapping.home;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


final class MyMap
    extends net.okapia.osid.jamocha.mapping.map.spi.AbstractMap
    implements org.osid.mapping.Map {
    
    private static final String ID = "urn:osid:okapia.net:identifiers:examples:map:home";
    private static final String NAME = "My Home";
    private static final String DESCRIPTION = "This is where I live.";


    MyMap() {
        setId(net.okapia.osid.primordium.id.URNId.valueOf(ID));
        setDisplayName(text(NAME));
        setDescription(text(DESCRIPTION));

        return;
    }


    org.osid.mapping.Location getLocation() {
        return (new net.okapia.osid.jamocha.builder.mapping.location.LocationBuilder()
                .id(getId())
                .genus(net.okapia.osid.primordium.type.BasicType.valueOf("location:city@okapia.net"))
                .displayName(text("Boston, MA"))
                .description(getDescription())
                .build());
    }
}
