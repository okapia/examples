//
// LocationLookupSession.java
//
//     LocationLookupSession for my home.
//
//
// Tom Coppeto
// 27 July 2019
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.examples.providers.mapping.roulette;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;

import net.okapia.examples.providers.mapping.roulette.MappingManager.Bankroll;
import net.okapia.examples.providers.mapping.roulette.MappingManager.Table;


final class LocationLookupSession
    extends net.okapia.osid.jamocha.mapping.spi.AbstractLocationLookupSession
    implements org.osid.mapping.LocationLookupSession {
    
    private final Bankroll bankroll;
    private final Table table;

    
    LocationLookupSession(org.osid.mapping.Map map, Bankroll bankroll, Table table) {
        nullarg(bankroll, "bankroll");
        nullarg(table, "table");

        this.bankroll = bankroll;
        this.table = table;

        setMap(map);

        return;
    }
    

    @OSID @Override
    public org.osid.mapping.Location getLocation(org.osid.id.Id locationId) 
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        if (!locationId.getAuthority().equals(RouletteLocation.AUTHORITY) ||
            !locationId.getIdentifierNamespace().equals(RouletteLocation.NAMESPACE)) {
            throw new org.osid.NotFoundException(locationId + " not found");
        }

        try {
            if (locationId.getIdentifier().equals("00")) {
                return (new RouletteLocation(locationId));
            }

            int n = Integer.parseInt(locationId.getIdentifier());
            if ((n >= 0) && (n <=36)) {
                return (new RouletteLocation(locationId));
            }
        } catch (NumberFormatException nfe) {
        }

        throw new org.osid.NotFoundException(locationId + " not found");
    }


    @OSID @Override
    public org.osid.mapping.LocationList getLocations() 
        throws org.osid.OperationFailedException {

        return (new RouletteLocationList(bankroll, table));
    }
}
