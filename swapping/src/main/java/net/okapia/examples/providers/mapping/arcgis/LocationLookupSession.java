//
// LocationLookupSession.java
//
//     LocationLookupSession for ArcGIS.
//
//
// Tom Coppeto
// 27 July 2019
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.examples.providers.mapping.arcgis;

import org.osid.binding.java.annotation.OSID;


final class LocationLookupSession
    extends net.okapia.osid.jamocha.mapping.spi.AbstractLocationLookupSession
    implements org.osid.mapping.LocationLookupSession {
    
    private final ArcgisMap map;

    
    LocationLookupSession(ArcgisMap map) {
        this.map = map;
        setMap(map);

        return;
    }
    
    
    @OSID @Override
    public org.osid.mapping.Location getLocation(org.osid.id.Id locationId) 
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {
        
        if (!locationId.getAuthority().equals(ArcgisCountryLocation.AUTHORITY) ||
            !locationId.getIdentifierNamespace().equals(ArcgisCountryLocation.NAMESPACE)) {
            throw new org.osid.NotFoundException(locationId + " not found");
        }

        return (queryByCode(locationId.getIdentifier()));
    }


    @OSID @Override
    public org.osid.mapping.LocationList getLocations() 
        throws org.osid.OperationFailedException {

        return (queryAll());
    }


    private org.osid.mapping.Location queryByCode(String code) 
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        try {
            java.net.URL url = new java.net.URL(map.getUrl() + "/" + code + "?f=pjson");
            java.net.URLConnection connection = url.openConnection();
            
            try (java.io.InputStream stream = url.openConnection().getInputStream()) {
                try (javax.json.JsonReader reader = javax.json.Json.createReader(stream)) {
                    javax.json.JsonObject jo = reader.readObject();
                    javax.json.JsonArray ja = jo.getJsonArray("countries");
                    
                    if (ja == null) {
                        throw new org.osid.OperationFailedException("no response from server");
                    }
                    
                    if (ja.size() == 0) {
                        throw new org.osid.NotFoundException(code + " not found");
                    }
                    
                    if (ja.size() > 1) {
                        throw new org.osid.OperationFailedException(code + " is not a unique identifier");
                    }
                    
                    return (ArcgisCountryLocation.valueOf(ja.getJsonObject(0)));
                }
            }
        } catch (Exception e) {
            throw new org.osid.OperationFailedException(e);
        }
    }


    private org.osid.mapping.LocationList queryAll()
        throws org.osid.OperationFailedException {

        try {
            java.net.URL url = new java.net.URL(map.getUrl() + "?f=pjson");
            java.net.URLConnection connection = url.openConnection();
            
            try (java.io.InputStream stream = url.openConnection().getInputStream()) {
                try (javax.json.JsonReader reader = javax.json.Json.createReader(stream)) {
                    javax.json.JsonObject jo = reader.readObject();
                    javax.json.JsonArray ja = jo.getJsonArray("countries");
                    
                    if (ja == null) {
                        throw new org.osid.OperationFailedException("no response from server");
                    }
                    
                    return (new ArcgisCountryLocationList(ja));
                }
            }
        } catch (Exception e) {
            throw new org.osid.OperationFailedException(e);
        }
    }
}
