//
// RouletteLocation.java
//
//     A location from a Roulette wheel.
//
//
// Tom Coppeto
// 27 July 2019
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.examples.providers.mapping.roulette;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


final class RouletteLocation
    extends net.okapia.osid.jamocha.mapping.location.spi.AbstractLocation
    implements org.osid.mapping.Location {
    
    static final String AUTHORITY = "Monet Carlo";
    static final String NAMESPACE = "roulette";


    RouletteLocation(org.osid.id.Id locationId) {
        nullarg(locationId, "location Id");

        setId(locationId);
        setDisplayName(text(locationId.getIdentifier()));
        setDescription(makeDescription(locationId.getIdentifier()));
        setSpatialUnit(new RouletteSpatialUnit(locationId.getIdentifier(), 
                                               getBounds(locationId.getIdentifier())));

        setGenusType(net.okapia.osid.primordium.type.BasicType.valueOf("location:roulette@Table 7"));

        return;
    }
    
    
    static org.osid.mapping.Location spin() {
        return (new RouletteLocation(new RouletteId(new RouletteSpin().getNumber())));
    }

    
    private org.osid.locale.DisplayText makeDescription(String number) {
        switch (number) {
        case "0":
        case "00":
            return (text("Uh oh."));
        case "1":
        case "7":
            return (text("Black. Odd. 1st 12. Left third."));
        case "2":
        case "8":
            return (text("Red. Even. 1st 12. Middle third."));
        case "3":
        case "9":
            return (text("Black. Odd. 1st 12. Right third."));
        case "4":
        case "10":
            return (text("Red. Even. 1st 12. Left third."));
        case "5":
        case "11":
            return (text("Black. Odd. 1st 12. Middle third."));
        case "6":
        case "12":
            return (text("Red. Even. 1st 12. Right third."));
        case "13":
        case "19":
            return (text("Black. Odd. 2nd 12. Left third."));
        case "14":
        case "20":
            return (text("Red. Even. 2nd 12. Middle third."));
        case "15":
        case "21":
            return (text("Black. Odd. 2nd 12. Right third."));
        case "16":
        case "22":
            return (text("Red. Even. 2nd 12. Left third."));
        case "17":
        case "23":
            return (text("Black. Odd. 2nd 12. Middle third."));
        case "18":
        case "24":
            return (text("Red. Even. 2nd 12. Right third."));
        case "25":
        case "31":
            return (text("Black. Odd. 3nd 12. Left third."));
        case "26":
        case "32":
            return (text("Red. Even. 3nd 12. Middle third."));
        case "27":
        case "33":
            return (text("Black. Odd. 3nd 12. Right third."));
        case "28":
        case "34":
            return (text("Red. Even. 3nd 12. Left third."));
        case "29":
        case "35":
            return (text("Black. Odd. 3nd 12. Middle third."));
        case "30":
        case "36":
            return (text("Red. Even. 3nd 12. Right third."));
        default:
            return (text("The wheel is rigged."));
        }
    }


    private int[] getBounds(String number) {
        switch (number) {
        case "6":
            return (new int[] {18, 21});
        case "21":
            return (new int[] {6, 33});
        case "33":
            return (new int[] {21, 16});
        case "16":
            return (new int[] {33, 4});
        case "4":
            return (new int[] {16, 23});
        case "23":
            return (new int[] {4, 35});
        case "35":
            return (new int[] {23, 14});
        case "14":
            return (new int[] {35, 2});
        case "2":
            return (new int[] {14, 0});
        case "28":
            return (new int[] {0, 9});
        case "9":
            return (new int[] {28, 26});
        case "26":
            return (new int[] {9, 30});
        case "30":
            return (new int[] {26, 11});
        case "11":
            return (new int[] {30, 7});
        case "7":
            return (new int[] {11, 20});
        case "20":
            return (new int[] {7, 32});
        case "32":
            return (new int[] {20, 17});
        case "17":
            return (new int[] {32, 5});
        case "5":
            return (new int[] {17, 22});
        case "22":
            return (new int[] {5, 34});
        case "34":
            return (new int[] {22, 15});
        case "15":
            return (new int[] {34, 3});
        case "3":
            return (new int[] {15, 24});
        case "24":
            return (new int[] {3, 36});
        case "36":
            return (new int[] {24, 13});
        case "13":
            return (new int[] {36, 1});
        case "1":
            return (new int[] {13, 0});
        case "0":
            return (new int[] {1, 27});
        case "27":
            return (new int[] {0, 10});
        case "10":
            return (new int[] {27, 25});
        case "25":
            return (new int[] {10, 29});
        case "29":
            return (new int[] {25, 12});
        case "12":
            return (new int[] {29, 8});
        case "8":
            return (new int[] {12, 19});
        case "19":
            return (new int[] {8, 31});
        case "31":
            return (new int[] {19, 18});
        default:
            return (new int[] {});
        }
    }


    static class RouletteSpin
        extends javax.swing.JDialog
        implements java.awt.event.ActionListener {

        private final RouletteWheel wheel;


        RouletteSpin() {
            super((java.awt.Frame) null, "Location Casino", true);
            setResizable(false);

            this.wheel = new RouletteWheel();
            wheel.addActionListener(this);
            add(this.wheel);

            java.awt.Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
            java.awt.Dimension window  = this.getSize();
            java.awt.Point point = new java.awt.Point ((screen.width  - window.width) / 2,
                                                       (screen.height - window.height - 250) / 2);
            this.setLocation(point);
            wheel.spin();

            pack();
            setVisible(true);

            return;
        }


        public void actionPerformed(java.awt.event.ActionEvent e) {
            javax.swing.JButton button = new javax.swing.JButton("Continue");
            button.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
            button.setDefaultCapable(true);
            getRootPane().setDefaultButton(button);
            add(button);

            button.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent e) {
                        dispose();
                    }
                });

            return;
        }


        public String getNumber() {
            return (this.wheel.getNumber());
        }        
    }


    public static class RouletteId
        extends net.okapia.osid.primordium.id.spi.AbstractId
        implements org.osid.id.Id {
        
        private static final long serialVersionUID = 123456789L;

        
        RouletteId(String number) {
            super(AUTHORITY, NAMESPACE, number);
        }

        
        public String formatLabel() {
            return (getIdentifier());
        }
    }

    
    public static class RouletteCoordinate 
        extends net.okapia.osid.primordium.mapping.spi.AbstractCoordinate
        implements org.osid.mapping.Coordinate {
        
        private static final org.osid.type.Type TYPE = net.okapia.osid.primordium.type.BasicType.valueOf("coordinate:roulette@okapia.net");
        private static final long serialVersionUID = 123456789L;


        RouletteCoordinate(int number) {
            super(TYPE, new java.math.BigDecimal[] { new java.math.BigDecimal(number) });
            return;
        }

        
        static RouletteCoordinate valueOf(String number)
            throws NumberFormatException {

            return (new RouletteCoordinate(Integer.parseInt(number)));
        }

        
        @OSIDBinding @Override
        public org.osid.mapping.Coordinate getFarthestBound() {
            return (this);
        }


        @OSIDBinding @Override
        public org.osid.mapping.Coordinate getClosestBound() {
            return (this);
        }


        @OSIDBinding @Override
        public boolean isSmaller(org.osid.mapping.Coordinate coordinate) {
            return (false);
        }


        @OSIDBinding @Override
        public boolean isLarger(org.osid.mapping.Coordinate coordinate) {
            return (false);
        }


        @OSIDBinding @Override
        public boolean isInclusive(org.osid.mapping.Coordinate coordinate) {
            return (!equals(coordinate));
        }


        @OSIDBinding @Override
        public boolean isExclusive(org.osid.mapping.Coordinate coordinate) {
            return (!equals(coordinate));
        }


        // ???

        @OSIDBinding @Override
        public boolean isCloser(org.osid.mapping.Coordinate coordinate, 
                                org.osid.mapping.Coordinate two) {
            return (false);
        }


        @OSIDBinding @Override
        public boolean isFarther(org.osid.mapping.Coordinate coordinate, 
                                 org.osid.mapping.Coordinate two) {
            return (false);
        }


        @OSIDBinding @Override
        public org.osid.mapping.SpatialUnit getOuterBound() {
            return (null);
        }

        
        @Override
        public String toString() {
            String ret = "";

            for (java.math.BigDecimal d : getValues()) {
                ret = ret + d + " ";
            }

            return (ret);
        }
    }


    public static class RouletteSpatialUnit
        extends net.okapia.osid.primordium.mapping.spi.AbstractSpatialUnit
        implements org.osid.mapping.SpatialUnit {

        private final java.util.Collection<org.osid.mapping.Coordinate> bounds = new java.util.ArrayList<>();

        RouletteSpatialUnit(String center, int[] bounds)
            throws NumberFormatException {

            super(RouletteCoordinate.valueOf(center));

            for (int b : bounds) {
                this.bounds.add(new RouletteCoordinate(b));
            }
        }


        @OSID @Override
        public org.osid.mapping.CoordinateList getBoundingCoordinates() {
            return (new net.okapia.osid.jamocha.mapping.coordinate.ArrayCoordinateList(this.bounds));
        }

        
        @OSID @Override
        public org.osid.type.TypeList getRecordTypes() {
            return (new net.okapia.osid.jamocha.nil.type.type.EmptyTypeList());
        }


        @OSID @Override
        public boolean hasRecordType(org.osid.type.Type type) {
            return (false);
        }


        @OSID @Override
        public org.osid.mapping.records.SpatialUnitRecord getSpatialUnitRecord(org.osid.type.Type type) {
            throw new org.osid.UnsupportedException("I don't have one of those.");
        }

        
        @OSIDBinding @Override
        public boolean isLarger(org.osid.mapping.SpatialUnit unit) {
            return (false);
        }


        @OSIDBinding @Override
        public boolean isSmaller(org.osid.mapping.SpatialUnit unit) {
            return (false);
        }


        @OSIDBinding @Override
        public boolean isInclusive(org.osid.mapping.SpatialUnit unit) {
            return (false);
        }


        @OSIDBinding @Override
        public boolean isExclusive(org.osid.mapping.SpatialUnit unit) {
            return (false);
        }


        @OSIDBinding @Override
        public org.osid.mapping.SpatialUnit getInnerBound() {
            return (this);
        }


        @OSIDBinding @Override
        public org.osid.mapping.SpatialUnit getOuterBound() {
            return (this);
        }


        @OSIDBinding @Override
        public int compareTo(org.osid.mapping.SpatialUnit unit) {
            if (this == unit) {
                return (0);
            }

            return (getCenterCoordinate().compareTo(unit.getCenterCoordinate()));
        }


        /// ???

        @OSIDBinding @Override
        public boolean isCloser(org.osid.mapping.SpatialUnit one, 
                                org.osid.mapping.SpatialUnit another) {

            return (false);
        }

        @OSIDBinding @Override
        public boolean isFarther(org.osid.mapping.SpatialUnit one, 
                                 org.osid.mapping.SpatialUnit another) {

            return (false);
        }
    }        
}
