//
// PiLocation.java
//
//     A place built upon Pi.
//
//
// Tom Coppeto
// 27 July 2019
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.examples.providers.mapping.pi;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


final class PiLocation
    extends net.okapia.osid.jamocha.mapping.location.spi.AbstractLocation
    implements org.osid.mapping.Location {
    
    static final String AUTHORITY = "Math Department";
    static final String NAMESPACE = "pi";


    PiLocation(int digits) {
        cardinalarg(digits, "digits");

        setId(new net.okapia.osid.primordium.id.BasicId(AUTHORITY, NAMESPACE, String.valueOf(digits)));
        java.math.BigDecimal pi = Pi.pi(digits);
        setDisplayName(text(pi.toString()));
        setDescription(text("PI to the " + ordinal(digits+1) + " digit."));
        setGenusType(net.okapia.osid.primordium.type.BasicType.valueOf("location:pi@okapia.net"));

        return;
    }


    public static String ordinal(int i) {
        String[] sufixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };

        switch (i % 100) {
        case 11:
        case 12:
        case 13:
            return (i + "th");
        default:
            return (i + sufixes[i % 10]);
        }
    }
}
