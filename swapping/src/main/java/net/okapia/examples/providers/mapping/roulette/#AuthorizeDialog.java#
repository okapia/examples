//
// Dialog.java
//
//     Pops up a dialog box for roulette.
//
//  
// Tom Coppeto
// OnTapSolutions
// 5 March 2005
//
//
// Copyright (c) 2005 Massachusetts Institute of Technology
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//
//
// $CVSHeader: $
//

package edu.mit.osidimpl.authorization.roulette;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import edu.mit.osidutil.swing.BlinkingLabel;
import edu.mit.osidutil.swing.RouletteWheel;


/**
 *  <p>
 *  Pops up a dialog to perform the roulette authorization.
 *  </p><p>
 *  CVS $Id: AuthorizeDialog.java,v 1.2 2006/04/13 20:15:00 tom Exp $
 *  </p>
 *  
 *  @author  Tom Coppeto
 *  @version $Revision: 1.2 $
 */

public class Dialog 
    extends JDialog
    implements ActionListener {

    protected JPanel panel;
    protected JMenu menu;
    protected BlinkingLabel selectionLabel;
    protected RouletteWheel wheel;
    protected int selection = 0;

    /**
     *  Constructor.
     */

    public Dialog(Frame owner) {
	super(owner, "Roulette", true);
	setResizable(false);
	displayInputWindow();
    }

    /**
     *  Gets the results.
     *
     *  @return true if authorized
     */

    public String getNumber() {
        return (wheel.getNumber());
    }


    /**
     *  Registered callback for the roulette wheel when the play
     *  is complete.
     *
     *  @param e the ActionEvent correspoding the event
     */

    public void actionPerformed(ActionEvent e) {
	String action = e.getActionCommand();
	
	if (action.equals("roulette")) {
	    authorize();
	} else {
	    selection = Integer.parseInt(action);
	    menu.setText(action);
	}
    }


    /*
     *  Creates the first inout screen
     */

    protected void displayInputWindow() {

	/* 
	 * panel inside the dialog
	 */

	JPanel outer = new JPanel();
	outer.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
	getContentPane().add(outer, BorderLayout.CENTER);

	panel = new JPanel();
	outer.add(panel);
	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEtchedBorder());

	panel.add(Box.createVerticalStrut(10));
	panel.add(Box.createHorizontalStrut(300));

	/*
	 * input label
	 */

	JPanel box = new JPanel();
	panel.add(box);
	JLabel label = new JLabel("Input your authorization code: ");	
	box.add(label);

	/*
	 *  input choice pulldown
	 */

	JMenuBar menuBar = new JMenuBar();
	box.add(menuBar);
	menuBar.setAlignmentX(Component.CENTER_ALIGNMENT);
	menuBar.setBorder(BorderFactory.createRaisedBevelBorder());

	menu = new JMenu("     ");
	menuBar.add(menu);
	menu.setHorizontalAlignment(SwingConstants.CENTER);
	add_selections(menu);

	/*
	 * vertical space
	 */

	panel.add(Box.createVerticalStrut(25));

	/*
	 * the login button
	 */

	JButton button = new JButton("login");
	button.setAlignmentX(Component.CENTER_ALIGNMENT);
	getRootPane().setDefaultButton(button);
	panel.add(button);

	button.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    login();
		}
	    });

	/*
	 * vertical space
	 */

	panel.add(Box.createVerticalStrut(10));

	/*
	 *  pack and show
	 */

	pack();
	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	Dimension window  = this.getSize();
	Point point = new Point ((screen.width  - window.width) / 2, 
				 (screen.height - window.height - 250) / 2);
	this.setLocation(point);
	setVisible(true);
    }


    /*
     * Creates the pulldown menu of choices 1-36
     */

    protected void add_selections(JMenu menu) {
	ButtonGroup group = new ButtonGroup();
	for (int i = 1; i <= 36; i++) {
	    JMenuItem item = new JRadioButtonMenuItem(Integer.toString(i));
	    menu.add(item);
	    group.add(item);
	    item.addActionListener(this);
	}
	return;
    }


    /*
     * This is the method used after the login button is pressed.
     * It modifies the panel to display the roulette wheel.
     */

    protected void login() {

	/*
	 *  make sure a number was selected
	 */

	if (selection == 0) {
	    panel.add(Box.createVerticalStrut(20));
	    JLabel label = new JLabel("make selection first");
	    label.setAlignmentX(Component.CENTER_ALIGNMENT);
	    label.setForeground(Color.red);
	    panel.add(label);
	    pack();
	    return;
	}

	/*
	 * remove the panel contents
	 */

	panel.removeAll();

	/*
	 * need a little spacing strcuture
	 */

	panel.add(Box.createVerticalStrut(10));
	panel.add(Box.createHorizontalStrut(300));

	/*
	 * display the "bet"
	 */

	JPanel box = new JPanel();
	panel.add(box);
	box.add(new JLabel("bet: "));
	this.selectionLabel = new BlinkingLabel(Integer.toString(selection) + " ");
	box.add(selectionLabel);
	
	selectionLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
	selectionLabel.setFont(new Font("Serif", Font.BOLD, 30));
	String color = RouletteWheel.getColor(Integer.toString(selection));

	if (color.equals("red")) {
	    selectionLabel.setForeground(Color.red);
	} else 	if (color.equals("green")) {
	    selectionLabel.setForeground(Color.green);
	}

	/*
	 * more visual padding
	 */

	panel.add(Box.createVerticalStrut(25));

	/*
	 * create and display the wheel
	 */

	wheel = new RouletteWheel();
	panel.add(wheel);
	wheel.addActionListener(this);

	/*
	 * pack and show
	 */

	pack();
	setVisible(true);

	/*
	 * give it a spin
	 */

	wheel.spin();

	return;
    }


    /*
     * this is called after the wheel stops spinning
     */

    protected void authorize() {

	/*
	 * the bet is complete
	 */

	selectionLabel.stopBlinking();

	/*
	 * vertical spacing
	 */

	panel.add(Box.createVerticalStrut(25));

	/*
	 * extend the panel to display the result
	 */

	BlinkingLabel blabel = new BlinkingLabel(" " + wheel.getNumber() + " ");
	blabel.setAlignmentX(Component.CENTER_ALIGNMENT);

	String color = wheel.getColor(wheel.getNumber());

	if (color.equals("red")) {
	    blabel.setForeground(Color.red);
	} else 	if (color.equals("green")) {
	    blabel.setForeground(Color.green);
	}

	blabel.setFont(new Font("Serif", Font.BOLD, 30));
	panel.add(blabel);
	
	panel.add(Box.createVerticalStrut(10));
	String s = Integer.toString(selection);
	if (s.equals(wheel.getNumber())) {
	    JLabel label = new JLabel("Authorized!");
	    label.setAlignmentX(Component.CENTER_ALIGNMENT);
	    panel.add(label);
	} else {
	    JLabel label = new JLabel("UNAUTHORIZED");	
	    label.setAlignmentX(Component.CENTER_ALIGNMENT);
	    panel.add(label);
	}
	panel.add(Box.createVerticalStrut(10));

	/*
	 * display the close dialog button
	 */

	JButton button = new JButton("Continue");
	button.setAlignmentX(Component.CENTER_ALIGNMENT);
	button.setDefaultCapable(true);
	getRootPane().setDefaultButton(button);
	panel.add(button);

	button.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    dispose();
		}
	    });

	/*
	 * vertical spacing
	 */

	panel.add(Box.createVerticalStrut(10));

	/* 
	 * pack it
	 */

	pack();

	return;
    }
}
