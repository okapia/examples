//
// ServiceProvider.java
//
//     Provider identity for Location Casino.
//
//
// Tom Coppeto
// Okapia
// 19 July 2019
//
//
// Copyright (c) 2019 Okpaia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.examples.providers.mapping.roulette;


public final class ServiceProvider
    extends net.okapia.osid.provider.spi.AbstractServiceProvider
    implements net.okapia.osid.provider.ServiceProvider {

    private static final String ID           = "urn:osid:okapia.net:identifiers:providers:examples:mapping:casino";
    private static final String DISPLAY_NAME = "My Home";
    private static final String DESCRIPTION  = "An OSID Mapping Provider for the Location Casino.";
    private static final String VERSION      = "0.1.0";
    private static final String RELEASE_DATE = "2019-07-27";

    
    ServiceProvider() {
        setServiceId(ID);
        setServiceName(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DISPLAY_NAME));
        setServiceDescription(net.okapia.osid.primordium.locale.text.eng.us.Plain.valueOf(DESCRIPTION));
        setImplementationVersion(VERSION);
        setReleaseDate(RELEASE_DATE);
        setProvider(net.okapia.osid.provider.Providers.OKAPIA.getProvider());

        return;
    }
}
