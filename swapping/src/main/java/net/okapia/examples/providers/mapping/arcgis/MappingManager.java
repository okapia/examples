//
// MappingManager.java
//
//     MappingManager for ArcGIS countries.
//
//
// Tom Coppeto
// 27 July 2019
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.examples.providers.mapping.arcgis;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


public final class MappingManager
    extends net.okapia.osid.jamocha.mapping.spi.AbstractMappingManager
    implements org.osid.mapping.MappingManager {

    private String geoUrl;
    private static final org.osid.id.Id GEO_URL_PARAMETER = net.okapia.osid.primordium.id.BasicId.valueOf("configuration:arcgisUrl@local");
    


    /**
     *  Constructs a @{code MappingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider}
     *          is <code>null</code>
     */

    public MappingManager() {
        super(new ServiceProvider());
        return;
    }


    /**
     *  Initializes this manager. A manager is initialized once at the time of
     *  creation.
     *
     *  @param  runtime the runtime environment
     *  @throws org.osid.ConfigurationErrorException an error with
     *          implementation configuration
     *  @throws org.osid.IllegalStateException this manager has already been
     *          initialized by the <code> OsidLoader </code>
     *  @throws org.osid.NullArgumentException <code> runtime </code> is
     *  <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public void initialize(org.osid.OsidRuntimeManager runtime)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        super.initialize(runtime);
        this.geoUrl = getConfigurationValue(GEO_URL_PARAMETER).getStringValue();

        return;
    }


    /**
     *  Tests if looking up locations is supported. 
     *
     *  @return <code> true </code> if location lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationLookup() {
        return (true);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  lookup service. 
     *
     *  @return a <code> LocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationLookupSession getLocationLookupSession()
        throws org.osid.OperationFailedException {

        return (new LocationLookupSession(new ArcgisMap(this.geoUrl)));
    }
}
