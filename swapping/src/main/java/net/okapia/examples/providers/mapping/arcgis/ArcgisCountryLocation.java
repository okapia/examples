//
// ArcgisCountryLocation.java
//
//     A country built from the ArcGIS data.
//
//
// Tom Coppeto
// 27 July 2019
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.examples.providers.mapping.arcgis;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


final class ArcgisCountryLocation
    extends net.okapia.osid.jamocha.mapping.location.spi.AbstractLocation
    implements org.osid.mapping.Location {
    
    static final String AUTHORITY = "ArcGIS";
    static final String NAMESPACE = "country";


    private ArcgisCountryLocation(String code, String name, String description) {
        nullarg(code, "code");
        nullarg(name, "name");
        nullarg(description, "description");

        setId(new net.okapia.osid.primordium.id.BasicId(AUTHORITY, NAMESPACE, code));
        setDisplayName(text(name));
        setDescription(text(description));
        setGenusType(net.okapia.osid.primordium.type.BasicType.valueOf("location:country@okapia.net"));

        return;
    }
    
    
    static org.osid.mapping.Location valueOf(javax.json.JsonObject json) 
        throws org.osid.OperationFailedException {

        String code = json.getString("abbr3");
        String name = json.getString("name");
        String continent = json.getString("continent");
        String description = name + " is on the continent of " + continent + ".";

        if ((code == null) || (name == null) || (continent == null)) {
            throw new org.osid.OperationFailedException("could not parse data");
        }

        return (new ArcgisCountryLocation(code, name, description));
    }
}
