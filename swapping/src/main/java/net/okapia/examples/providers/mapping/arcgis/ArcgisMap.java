//
// ArcgisMap.java
//
//     A catalog for ArcGIS.
//
//
// Tom Coppeto
// 27 July 2019
//
// Copyright (c) 2019 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.examples.providers.mapping.arcgis;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.primordium.locale.text.eng.us.Plain.text;


final class ArcgisMap
    extends net.okapia.osid.jamocha.mapping.map.spi.AbstractMap
    implements org.osid.mapping.Map {
    
    private static final org.osid.id.Id ID = net.okapia.osid.primordium.id.URNId.valueOf("urn:osid:okapia.net:identifiers:examples:map:ArcGIS");
    private static final org.osid.locale.DisplayText NAME = text("ArcGIS Countries");
    private static final org.osid.locale.DisplayText DESCRIPTION = text("ArcGIS is a geographic information system (GIS) for working with maps and geographic information. It is used for creating and using maps, compiling geographic data, analyzing mapped information, sharing and discovering geographic information, using maps and geographic information in a range of applications, and managing geographic information in a database.");
    
    private final String url;


    ArcgisMap(String url) {
        nullarg(url, "url");

        setId(ID);
        setDisplayName(NAME);
        setDescription(DESCRIPTION);

        this.url = url;

        return;
    }


    String getUrl() {
        return (this.url);
    }
}
