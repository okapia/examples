package net.okapia.examples.apps;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

import java.io.InputStream;
import java.util.Properties;

import org.osid.*;
import org.osid.mapping.*;

import net.okapia.osid.kilimanjaro.BootLoader;
import net.okapia.osid.kilimanjaro.OsidVersions;

import static java.lang.System.out;


public class LocationsInTable
    extends JPanel {

    private static final String[] COLUMNS = {"Location",
                                             "Type",
                                             "Coordinate",
                                             "Bounding Coordinates"};
    private final DefaultTableModel model = new DefaultTableModel();
    private final JTable table = new JTable(model);

    
    public LocationsInTable() {
        super(new GridLayout(1, 0));
        
        for (String s : COLUMNS) {
            this.model.addColumn(s);
        }

        this.table.setFillsViewportHeight(true);
        this.table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        add(new JScrollPane(this.table));
 
        return;
    }


    private void addRow(String location, String type, String coordinate, String bounds) {
        this.model.addRow(new Object[] {location, type, coordinate, bounds});
        return;
    }


    public static void main(String... args) {
        try {
            String impl;
            if (args.length == 0) {
                InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("Locations.properties");
                Properties props = new Properties();
                props.load(is);
                impl = props.getProperty("provider");
                if (impl == null) {
                    System.err.println("no OSID Provider!");
                    System.exit(0);
                }
            } else {
                impl = args[0];
            }
                       
            BootLoader loader = new BootLoader();
            try (OsidRuntimeManager runtime = loader.getRuntimeManager("Locations")) {
                try (MappingManager mgr = (MappingManager) runtime.getManager(OSID.MAPPING, impl,
                                                                              OsidVersions.V3_0_0.getVersion())) {
                        
                     try (LocationLookupSession session = mgr.getLocationLookupSession()) {

                         JFrame frame = new JFrame("SimpleTableDemo");
                         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                         frame.setLayout(new BorderLayout());

                         JPanel panel = new JPanel();
                         panel.setLayout(new BorderLayout());
                         frame.add(panel);

                         JPanel header = new JPanel();
                         header.setLayout(new BorderLayout());
                         panel.add(header, BorderLayout.NORTH);
                         
                         header.add(new JLabel(session.getMap().getDisplayName().toString(),
                                               SwingConstants.CENTER), BorderLayout.NORTH);

                         header.add(new JLabel(session.getMap().getDescription().toString(),
                                               SwingConstants.CENTER), BorderLayout.SOUTH);

                         LocationsInTable table = new LocationsInTable();
                         table.setOpaque(true);
                         panel.add(table);

                         frame.pack();
                         frame.setVisible(true);
 
                         try (LocationList locations = session.getLocations()) {
                             while (locations.hasNext()) {
                                 String coordinate = "";
                                 String bounds = "";
                                 Location location = locations.getNextLocation();
                                 
                                 if (location.hasSpatialUnit()) {
                                     SpatialUnit unit = location.getSpatialUnit();
                                     coordinate =  unit.getCenterCoordinate().toString();
                                     try (CoordinateList coordinates = unit.getBoundingCoordinates()) {
                                         while (coordinates.hasNext()) {
                                             bounds += coordinates.getNextCoordinate().toString();
                                         }
                                     }                                     
                                 }    
                                 table.addRow(location.getDisplayName().toString(), 
                                              location.getGenusType().toString(),
                                              coordinate, bounds);
                             }
                         }
                     }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    
}
                    
        
