package net.okapia.examples.apps;

import java.io.InputStream;
import java.util.Properties;

import org.osid.*;
import org.osid.mapping.*;

import net.okapia.osid.kilimanjaro.BootLoader;
import net.okapia.osid.kilimanjaro.OsidVersions;

import static java.lang.System.out;


public class Locations {

    public static void main(String... args) {
        try {
            String impl;
            if (args.length == 0) {
                InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("Locations.properties");
                Properties props = new Properties();
                props.load(is);
                impl = props.getProperty("provider");
                if (impl == null) {
                    System.err.println("no OSID Provider!");
                    System.exit(0);
                }
            } else {
                impl = args[0];
            }

            BootLoader loader = new BootLoader();
            try (OsidRuntimeManager runtime = loader.getRuntimeManager("Locations")) {
                try (MappingManager mgr = (MappingManager) runtime.getManager(OSID.MAPPING, impl,
                                                                              OsidVersions.V3_0_0.getVersion())) {
                        
                     try (LocationLookupSession session = mgr.getLocationLookupSession()) {

                         try (LocationList locations = session.getLocations()) {
                             while (locations.hasNext()) {
                                 Location location = locations.getNextLocation();
                                 out.println("Id:          " + location.getId());
                                 out.println("Type:        " + location.getGenusType());
                                 out.println("Name:        " + location.getDisplayName());
                                 out.println("Description: " + location.getDescription());
                                 
                                 if (location.hasSpatialUnit()) {
                                     SpatialUnit unit = location.getSpatialUnit();
                                     out.println("Coordinate:  " + unit.getCenterCoordinate());
                                     try (CoordinateList coordinates = unit.getBoundingCoordinates()) {
                                         out.print("Bounds:      ");
                                         while (coordinates.hasNext()) {
                                             out.print(coordinates.getNextCoordinate());
                                         }
                                         out.println();
                                     }
                                 }

                                 out.println("=================================");
                             }
                         }
                     }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.exit(0);
    }    
}
                    
        
